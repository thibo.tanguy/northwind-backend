using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;

namespace northwind_backend.App.Mappers;

public abstract class CategoryMapper
{
    public static CategoryDto MapCategoryToCategoryDto(Category category)
    {
        return new CategoryDto
        {
            Id = category.Id,
            CategoryName = category.CategoryName,
            Description = category.Description,
            // Picture = category.Picture
        };
    }
}