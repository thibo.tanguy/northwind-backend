using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;

namespace northwind_backend.App.Mappers;

public abstract class EmployeeMapper
{
    public static EmployeeDto MapEmployeeToEmployeeDetailsDto(Employee employee)
    {
        if (employee.Superieur != null)
            return new EmployeeDto
            {
                Id = employee.Id,
                ReportTo = employee.ReportTo,
                LastName = employee.LastName,
                FirstName = employee.FirstName,
                Title = employee.Title,
                TitleOfCourtesy = employee.TitleOfCourtesy,
                BirthDate = employee.BirthDate,
                HireDate = employee.HireDate,
                Address = employee.Address,
                City = employee.City,
                Region = employee.Region,
                PostalCode = employee.PostalCode,
                Country = employee.Country,
                HomePhone = employee.HomePhone,
                Extension = employee.Extension,
                Notes = employee.Notes,
                PhotoPath = employee.PhotoPath,
                Superieur = MapEmployeeToEmployeeDto(employee.Superieur)
            };

        return MapEmployeeToEmployeeDto(employee);
    }

    public static EmployeeMinimalDto MapEmployeeToEmployeeMinimalDto(Employee employee)
    {
        return new EmployeeMinimalDto
        {
            Id = employee.Id,
            Lastname = employee.LastName,
            Firstname = employee.FirstName
        };
    }

    private static EmployeeDto MapEmployeeToEmployeeDto(Employee employee)
    {
        return new EmployeeDto
        {
            Id = employee.Id,
            ReportTo = employee.ReportTo,
            LastName = employee.LastName,
            FirstName = employee.FirstName,
            Title = employee.Title,
            TitleOfCourtesy = employee.TitleOfCourtesy,
            BirthDate = employee.BirthDate,
            HireDate = employee.HireDate,
            Address = employee.Address,
            City = employee.City,
            Region = employee.Region,
            PostalCode = employee.PostalCode,
            Country = employee.Country,
            HomePhone = employee.HomePhone,
            Extension = employee.Extension,
            Notes = employee.Notes,
            PhotoPath = employee.PhotoPath,
        };
    }
}