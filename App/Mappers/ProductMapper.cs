using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;

namespace northwind_backend.App.Mappers;

public abstract class ProductMapper
{
    public static ProductDto MapProductToProductDto(Product product)
    {
        if (product is { Category: not null, Supplier: not null })
        {
            return new ProductDto
            {
                Id = product.Id,
                ProductName = product.ProductName,
                QuantityPerUnit = product.QuantityPerUnit,
                UnitPrice = product.UnitPrice,
                UnitsInStock = product.UnitsInStock,
                UnitsOnOrder = product.UnitsOnOrder,
                ReorderLevel = product.ReorderLevel,
                Discontinued = product.Discontinued,
                Category = CategoryMapper.MapCategoryToCategoryDto(product.Category),
                Supplier = SupplierMapper.MapSupplierToSupplierDto(product.Supplier)
            };
        }

        return new ProductDto
        {
            Id = product.Id,
            ProductName = product.ProductName,
            QuantityPerUnit = product.QuantityPerUnit,
            UnitPrice = product.UnitPrice,
            UnitsInStock = product.UnitsInStock,
            UnitsOnOrder = product.UnitsOnOrder,
            ReorderLevel = product.ReorderLevel,
            Discontinued = product.Discontinued
        };
    }
}