using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;

namespace northwind_backend.App.Mappers;

public abstract class CustomerMappper
{
    public static CustomerDto MapCustomerToCustomerDto(Customer customer)
    {
        return new CustomerDto
        {
            CompanyName = customer.CompanyName,
            ContactName = customer.ContactName,
            ContactTitle = customer.ContactTitle,
            Address = customer.Address,
            City = customer.City,
            Region = customer.Region,
            PostalCode = customer.PostalCode,
            Country = customer.Country,
            Phone = customer.Phone,
            Fax = customer.Fax
        };
    }
}