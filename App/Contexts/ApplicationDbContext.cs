﻿using Microsoft.EntityFrameworkCore;
using northwind_backend.App.Models.Entities;

namespace northwind_backend.App.Contexts;

public class ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : DbContext(options)
{
    public DbSet<Category> Categories { get; set; }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Employee> Employees { get; set; }
    public DbSet<EmployeeTerritories> EmployeeTerritories { get; set; }
    public DbSet<Order> Orders { get; set; }
    public DbSet<OrderDetails> OrderDetails { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<Region> Regions { get; set; }
    public DbSet<Shipper> Shippers { get; set; }
    public DbSet<Supplier> Suppliers { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Configurer la clé primaire composée pour OrderDetail
        modelBuilder.Entity<OrderDetails>()
            .HasKey(orderDetail => new { orderDetail.OrderId, orderDetail.ProductId });

        // Configurer la relation un-à-un entre Order et OrderDetail
        modelBuilder.Entity<OrderDetails>()
            .HasOne(orderDetail => orderDetail.Order)
            .WithOne(order => order.OrderDetails)
            .HasForeignKey<OrderDetails>(orderDetail => orderDetail.OrderId);

        // Configurer la relation plusieurs-à-un entre Product et OrderDetail
        modelBuilder.Entity<OrderDetails>()
            .HasOne(orderDetail => orderDetail.Product)
            .WithMany(product => product.OrderDetails)
            .HasForeignKey(orderDetail => orderDetail.ProductId);
    }
}