using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Services;

public class OrdersService(IOrdersRepository ordersRepository)
{
    public async Task<OrderTotalDto[]> GetOrdersWithTotalAmount()
    {
        return await ordersRepository.GetOrdersWithTotalAmountAsync();
    }
}