using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Services;

public class CategoriesService(ICategoriesRepository categoriesRepository)
{
    public async Task<CategoryProductCountDto[]> GetCategoriesWithProductCountGreaterThan(int productCount)
    {
        return await categoriesRepository.GetCategoriesWithProductCountGreaterThanAsync(productCount);
    }
}