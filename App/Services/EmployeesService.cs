using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Services;

public class EmployeesService(IEmployeesRepository employeesRepository)
{
    public Task<Employee[]> FindAll()
    {
        return employeesRepository.GetAllAsync();
    }
    
    public async Task<(EmployeeMinimalDto EmployeeWithMaxSales, EmployeeMinimalDto EmployeeWithMinSales)> GetEmployeesWithMinMaxSalesAsync()
    {
        return await employeesRepository.GetEmployeesWithMinMaxSalesAsync();
    }
}