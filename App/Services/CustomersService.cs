using northwind_backend.App.Models.Entities;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Services;

public class CustomersService(ICustomersRepository customersRepository)
{
    public async Task<Customer[]> GetCustomersWithoutOrdersAsync()
    {
        return await customersRepository.GetCustomersWithoutOrdersAsync();
    }
    
    public async Task<Customer[]> GetCustomersWithoutOrdersBadAsync()
    {
        return await customersRepository.GetCustomersWithoutOrdersBadAsync();
    }
}