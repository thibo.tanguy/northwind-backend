using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Services;

public class OrderDetailsService(IOrderDetailsRepository orderDetailsRepository)
{
    public async Task<OrderDetailsTotalDto[]> GetOrderDetailsWithTotalAsync()
    {
        return await orderDetailsRepository.GetOrderDetailsWithTotalAsync();
    }
}