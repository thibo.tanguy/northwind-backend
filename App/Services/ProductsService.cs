using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Services;

public class ProductsService(IProductsRepository productsRepository)
{
    public async Task<Product[]> FindProductsByCategories(CategoryDto[] categories)
    {
        return await productsRepository.GetProductsByCategoriesDtoAsync(categories);
    }

    public async Task<ProductWithAverageDto[]> FindProductsWithAveragePrice()
    {
        return await productsRepository.GetProductsWithAveragePriceAsync();
    }
}