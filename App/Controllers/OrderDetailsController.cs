using Microsoft.AspNetCore.Mvc;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Services;

namespace northwind_backend.App.Controllers;

[Route("api/[controller]")]
[ApiController]
public class OrderDetailsController(OrderDetailsService orderDetailsService): ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<OrderDetailsTotalDto[]>> GetOrderDetailsWithTotalAsync()
    {
        var orderDetails = await orderDetailsService.GetOrderDetailsWithTotalAsync();
        
        return Ok(orderDetails);
    }
}