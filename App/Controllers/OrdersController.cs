using Microsoft.AspNetCore.Mvc;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Services;

namespace northwind_backend.App.Controllers;

[Route("api/[controller]")]
[ApiController]
public class OrdersController(OrdersService ordersService) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<OrderTotalDto[]>> GetOrdersWithTotalAmount()
    {
        var orders = await ordersService.GetOrdersWithTotalAmount();
        
        return Ok(orders);
    }
}