using Microsoft.AspNetCore.Mvc;
using northwind_backend.App.Mappers;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Services;

namespace northwind_backend.App.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CustomersController(CustomersService customersService) : ControllerBase
{
    [HttpGet("no-orders")]
    public async Task<ActionResult<CustomerDto[]>> GetCustomersWithoutOrders()
    {
        var customers = await customersService.GetCustomersWithoutOrdersAsync();
        var customersDto = customers.Select(CustomerMappper.MapCustomerToCustomerDto).ToArray();
        
        return Ok(customersDto);
    }
    
    [HttpGet("no-orders/bad")]
    public async Task<ActionResult<CustomerDto[]>> GetCustomersWithoutOrdersBad()
    {
        var customers = await customersService.GetCustomersWithoutOrdersBadAsync();
        var customersDto = customers.Select(CustomerMappper.MapCustomerToCustomerDto).ToArray();
        
        return Ok(customersDto);
    }
}