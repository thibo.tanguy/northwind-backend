using Microsoft.AspNetCore.Mvc;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Services;

namespace northwind_backend.App.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CategoriesController(CategoriesService categoriesService) : ControllerBase
{
    
    [HttpGet("count/{count:int}")]
    public async Task<ActionResult<CategoryProductCountDto[]>> GetCategoriesWithProductCountGreaterThan(int count)
    {
        var categories = await categoriesService.GetCategoriesWithProductCountGreaterThan(count);
        
        return Ok(categories);
    }
}