using Microsoft.AspNetCore.Mvc;
using northwind_backend.App.Mappers;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Services;

namespace northwind_backend.App.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ProductsController(ProductsService productsService) : ControllerBase
{
    [HttpPost("categories")]
    public async Task<ActionResult<ProductDto[]>> GetProductsFromCategories([FromBody] CategoryDto[] categories)
    {
        var products = await productsService.FindProductsByCategories(categories);
        var productsDto = products.Select(ProductMapper.MapProductToProductDto).ToList();

        return Ok(productsDto);
    }
    
    [HttpGet("average")]
    public async Task<ActionResult<ProductDto[]>> GetProductsWithAveragePrice()
    {
        var productsDto = await productsService.FindProductsWithAveragePrice();

        return Ok(productsDto);
    }
}