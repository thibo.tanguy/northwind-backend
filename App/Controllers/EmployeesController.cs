using Microsoft.AspNetCore.Mvc;
using northwind_backend.App.Mappers;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;
using northwind_backend.App.Services;

namespace northwind_backend.App.Controllers;

[Route("api/[controller]")]
[ApiController]
public class EmployeesController(EmployeesService employeesService) : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult<EmployeeDto[]>> GetAllEmployees()
    {
        var employees = await employeesService.FindAll();
        var employeesDto = employees.Select(EmployeeMapper.MapEmployeeToEmployeeDetailsDto).ToList();
        
        return Ok(employeesDto);
    }
    
    [HttpGet("minmax")]
    public async Task<ActionResult<(EmployeeMinimalDto EmployeeWithMaxSales, EmployeeMinimalDto EmployeeWithMinSales)>> GetEmployeesWithMinMaxSalesAsync()
    {
        var employees = await employeesService.GetEmployeesWithMinMaxSalesAsync();
        
        return Ok(employees);
    }
}