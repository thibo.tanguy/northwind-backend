using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table("Products")]
public class Product
{
    [Key]
    [Required]
    [Column("ProductID")]
    public int Id { get; set; }
    
    [Column("CategoryID")]
    public required int CategoryId { get; set; }
    
    [Column("SupplierID")]
    public required int SupplierId { get; set; }
    
    [Column("ProductName")] [MaxLength(50)] public required string ProductName { get; set; }
    [Column("QuantityPerUnit")] [MaxLength(50)] public string? QuantityPerUnit { get; set; }
    [Column("UnitPrice")] public decimal? UnitPrice { get; set; }
    [Column("UnitsInStock")] public short? UnitsInStock { get; set; }
    [Column("UnitsOnOrder")] public short? UnitsOnOrder { get; set; }
    [Column("ReorderLevel")] public short? ReorderLevel { get; set; }
    [Column("Discontinued")] public bool? Discontinued { get; set; }
    
    [ForeignKey("CategoryId")]
    public required Category Category { get; set; }
    
    [ForeignKey("SupplierId")]
    public required Supplier Supplier { get; set; }
    
    public ICollection<OrderDetails> OrderDetails = new List<OrderDetails>();
}