using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table("Shipper")]
public class Shipper
{
    [Key]
    [Required]
    [Column("ShipperID")]
    public int Id { get; set; }
    
    [Column("CompanyName")] [MaxLength(50)] public string? RegionDescription { get; set; }
    [Column("Phone")] [MaxLength(50)] public string? Phone { get; set; }
}