using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table(("EmployeeTerritories"))]
public class EmployeeTerritories
{
    [Key]
    [Required]
    [Column("EmployeeID")]
    public int Id { get; set; }

}