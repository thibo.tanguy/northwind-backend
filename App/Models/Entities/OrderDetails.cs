using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table("OrderDetails")]
public class OrderDetails
{
    [Required]
    [Column("OrderID")]
    public int OrderId { get; set; }
    
    [Column("ProductID")]
    [Required]
    public required int ProductId { get; set; }
    
    [Column("UnitPrice")] public required short UnitPrice { get; set; }
    [Column("Quantity")] public required short Quantity { get; set; }
    [Column("UnitsInStock")] public required float UnitsInStock { get; set; }
    [Column("Discount")] [MaxLength(50)] public required float Discount { get; set; }
    
    public required Product Product { get; set; }
    public required Order Order { get; set; }
}