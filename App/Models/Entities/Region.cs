using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table("Regions")]
public class Region
{
    [Key]
    [Required]
    [Column("RegionID")]
    public int Id { get; set; }
    
    [Column("RegionDescription")] [MaxLength(50)] public string? RegionDescription { get; set; }
}