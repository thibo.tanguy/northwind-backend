using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table(("Territories"))]
public class Territory
{
    [Key]
    [Required]
    [Column("TerritoryID")]
    public required string Id { get; set; }
    
    [Column("RegionID")]
    [Required]
    public required int RegionId { get; set; }
    
    [Column("TerritoryDescription")] [MaxLength(50)] public string? TerritoryDescription { get; set; }
    
    [ForeignKey("RegionId")]
    public required Region Region { get; set; }
}