using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table("Categories")]
public class Category
{
    [Key]
    [Required]
    [Column("CategoryID")]
    public int Id { get; set; }

    [Column("CategoryName")] [MaxLength(50)] public required string CategoryName { get; set; }
    [Column("Description")]public string? Description { get; set; }
    // [Column("Picture")] [MaxLength(50)] public string? Picture { get; set; }
}