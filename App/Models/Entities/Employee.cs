using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table(("Employees"))]
public class Employee
{
    [Key]
    [Required]
    [Column("EmployeeID")]
    public required int Id { get; set; }
    
    [Column("ReportsTo")]
    public int? ReportTo { get; set; }
    
    [Column("LastName")] [MaxLength(50)] public required string LastName { get; set; }
    [Column("FirstName")] [MaxLength(50)] public required string FirstName { get; set; }
    [Column("Title")] [MaxLength(50)] public string? Title { get; set; }
    [Column("TitleOfCourtesy")] [MaxLength(50)] public string? TitleOfCourtesy { get; set; }
    [Column("BirthDate")] [MaxLength(50)] public DateTime? BirthDate { get; set; }
    [Column("HireDate")] [MaxLength(50)] public DateTime? HireDate { get; set; }
    [Column("Address")] [MaxLength(60)] public string? Address { get; set; }
    [Column("City")] [MaxLength(50)] public string? City { get; set; }
    [Column("Region")] [MaxLength(50)] public string? Region { get; set; }
    [Column("PostalCode")] [MaxLength(50)] public string? PostalCode { get; set; }
    [Column("Country")] [MaxLength(50)] public string? Country { get; set; }
    [Column("HomePhone")] [MaxLength(50)] public string? HomePhone { get; set; }
    [Column("Extension")] [MaxLength(50)] public string? Extension { get; set; }
    [Column("Notes")] [MaxLength(50)] public string? Notes { get; set; }
    [Column("PhotoPath")] [MaxLength(255)] public string? PhotoPath { get; set; }
    
    [ForeignKey("ReportTo")]
    public Employee? Superieur { get; set; }
}