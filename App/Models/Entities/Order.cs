using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table("Orders")]
public class Order
{
    [Key]
    [Required]
    [Column("OrderID")]
    public required int Id { get; set; }
    
    [Column("CustomerID")]
    [MaxLength(10)]
    [Required]
    public required string CustomerId { get; set; }
    
    [Column("EmployeeID")]
    [Required]
    public required int EmployeeId { get; set; }
    
    [Column("OrderDate")] public DateTime? OrderDate { get; set; }
    [Column("RequiredDate")] public DateTime? RequiredDate { get; set; }
    // [Column("Freight")] public decimal? Freight { get; set; }
    [Column("ShipName")] [MaxLength(50)] public string? ShipName { get; set; }
    [Column("ShipAddress")] [MaxLength(60)] public string? ShipAddress { get; set; }
    [Column("ShipCity")] [MaxLength(50)] public string? ShipCity { get; set; }
    [Column("ShipRegion")] [MaxLength(50)] public string? ShipRegion { get; set; }
    [Column("ShipPostalCode")] [MaxLength(50)] public string? ShipPostalCode { get; set; }
    [Column("ShipCountry")] [MaxLength(60)] public string? ShipCountry { get; set; }
    
    [ForeignKey("CustomerId")]
    public required Customer Customer { get; set; }
    
    [ForeignKey("EmployeeId")]
    public required Employee Employee { get; set; }
    
    public required OrderDetails OrderDetails { get; set; }
}