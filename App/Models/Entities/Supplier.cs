using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace northwind_backend.App.Models.Entities;

[Table("Suppliers")]
public class Supplier
{
    [Key]
    [Required]
    [Column("SupplierID")]
    public int Id { get; set; }
    
    [Column("CompanyName")] [MaxLength(50)] public string? CompanyName { get; set; }
    [Column("ContactName")] [MaxLength(50)] public string? ContactName { get; set; }
    [Column("ContactTitle")] [MaxLength(50)] public string? ContactTitle { get; set; }
    [Column("Address")] [MaxLength(50)] public string? Address { get; set; }
    [Column("City")] [MaxLength(50)] public string? City { get; set; }
    [Column("Region")] [MaxLength(50)] public string? Region { get; set; }
    [Column("PostalCode")] [MaxLength(50)] public string? PostalCode { get; set; }
    [Column("Country")] [MaxLength(50)] public string? Country { get; set; }
    [Column("Phone")] [MaxLength(50)] public string? Phone { get; set; }
    [Column("Fax")] [MaxLength(50)] public string? Fax { get; set; }
}