namespace northwind_backend.App.Models.Dtos;

public class CategoryProductCountDto
{
    public required int CategoryId;
    public required string CategoryName;
    public required int ProductCount;
}