namespace northwind_backend.App.Models.Dtos;

public class OrderTotalDto
{
    public int OrderId;
    public decimal TotalAmount;
}