namespace northwind_backend.App.Models.Dtos;

public class CategoryDto
{
    public int Id { get; set; }
    public string? CategoryName { get; set; }
    public string? Description { get; set; }
    // public string? Picture { get; set; }
}