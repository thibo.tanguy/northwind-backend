namespace northwind_backend.App.Models.Dtos;

public class ProductWithAverageDto
{
    public required int ProductId { get; set; }
    public required string ProductName { get; set; }
    public required decimal AveragePrice { get; set; }
}