namespace northwind_backend.App.Models.Dtos;

public class EmployeeMinimalDto
{
    public required int Id;
    public required string Lastname;
    public required string Firstname;
}