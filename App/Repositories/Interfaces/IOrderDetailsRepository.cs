using northwind_backend.App.Models.Dtos;

namespace northwind_backend.App.Repositories.Interfaces;

public interface IOrderDetailsRepository
{
    Task<OrderDetailsTotalDto[]> GetOrderDetailsWithTotalAsync();
}