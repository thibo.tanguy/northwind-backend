using northwind_backend.App.Models.Entities;

namespace northwind_backend.App.Repositories.Interfaces;

public interface ICustomersRepository
{
    Task<Customer[]> GetCustomersWithoutOrdersAsync();
    Task<Customer[]> GetCustomersWithoutOrdersBadAsync();
}