using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;

namespace northwind_backend.App.Repositories.Interfaces;

public interface IEmployeesRepository
{
    public Task<Employee[]> GetAllAsync();
    Task<(EmployeeMinimalDto EmployeeWithMaxSales, EmployeeMinimalDto EmployeeWithMinSales)> GetEmployeesWithMinMaxSalesAsync();
}