using northwind_backend.App.Models.Dtos;

namespace northwind_backend.App.Repositories.Interfaces;

public interface ICategoriesRepository
{
    Task<CategoryProductCountDto[]> GetCategoriesWithProductCountGreaterThanAsync(int productCount);
}