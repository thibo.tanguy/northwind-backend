using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;

namespace northwind_backend.App.Repositories.Interfaces;

public interface IProductsRepository
{
    Task<Product[]> GetProductsByCategoriesDtoAsync(CategoryDto[] categories);
    Task<ProductWithAverageDto[]> GetProductsWithAveragePriceAsync();
}