using Microsoft.EntityFrameworkCore;
using northwind_backend.App.Contexts;
using northwind_backend.App.Mappers;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Repositories.Implementations;

public class EmployeesRepository(ApplicationDbContext dbContext) : IEmployeesRepository
{
    public Task<Employee[]> GetAllAsync()
    {
        return dbContext.Employees
            .Include(employee => employee.Superieur)
            .ToArrayAsync();
    }
    
    public async Task<(EmployeeMinimalDto EmployeeWithMaxSales, EmployeeMinimalDto EmployeeWithMinSales)> GetEmployeesWithMinMaxSalesAsync()
    {
        var employeeSales = await dbContext.Orders
            .GroupBy(order => new
            {
                order.Employee.Id,
                order.Employee.LastName,
                order.Employee.FirstName
            })
            .Select(group => new
            {
                Employee = new Employee
                {
                    Id = group.Key.Id,
                    LastName = group.Key.LastName,
                    FirstName = group.Key.FirstName
                },
                TotalSales = group.Sum(order => order.OrderDetails.UnitPrice * order.OrderDetails.Quantity * (1 - (decimal)order.OrderDetails.Discount))
            })
            .OrderBy(sales => sales.TotalSales)
            .ToListAsync();

        var employeeWithMinSales = employeeSales.First().Employee;
        var employeeWithMaxSales = employeeSales.Last().Employee;

        var employeeDtoWithMinSales = EmployeeMapper.MapEmployeeToEmployeeMinimalDto(employeeWithMinSales);
        var employeeDtoWithMaxSales = EmployeeMapper.MapEmployeeToEmployeeMinimalDto(employeeWithMaxSales);

        return (employeeDtoWithMaxSales, employeeDtoWithMinSales);
    }
}