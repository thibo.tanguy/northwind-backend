using Microsoft.EntityFrameworkCore;
using northwind_backend.App.Contexts;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Repositories.Implementations;

public class OrderDetailsRepository(ApplicationDbContext dbContext) : IOrderDetailsRepository
{
    public async Task<OrderDetailsTotalDto[]> GetOrderDetailsWithTotalAsync()
    {
        var orderDetailsWithTotal = await dbContext.OrderDetails
            .Select(od => new OrderDetailsTotalDto
            {
                OrderId = od.OrderId,
                ProductId = od.ProductId,
                UnitPrice = od.UnitPrice,
                Quantity = od.Quantity,
                Discount = od.Discount,
                Total = od.Quantity * od.UnitPrice * (1 - (decimal)od.Discount)
            })
            .ToArrayAsync();

        return orderDetailsWithTotal;
    }
}