using Microsoft.EntityFrameworkCore;
using northwind_backend.App.Contexts;
using northwind_backend.App.Models.Entities;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Repositories.Implementations;

public class CustomersRepository(ApplicationDbContext dbContext) : ICustomersRepository
{
    public async Task<Customer[]> GetCustomersWithoutOrdersBadAsync()
    {
        var customersWithoutOrders = await dbContext.Customers
            .Where(customer => !dbContext.Orders
                .Select(order => order.CustomerId)
                .Distinct()
                .Contains(customer.Id))
            .ToArrayAsync();

        return customersWithoutOrders;
    }

    public async Task<Customer[]> GetCustomersWithoutOrdersAsync()
    {
        var customersWithoutOrders = await dbContext.Customers
            .GroupJoin(
                dbContext.Orders,
                customer => customer.Id,
                order => order.CustomerId,
                (customer, orders) => new { Customer = customer, Orders = orders })
            .SelectMany(
                x => x.Orders.DefaultIfEmpty(),
                (x, order) => new { Customer = x.Customer, Order = order })
            .Where(x => x.Order == null)
            .Select(x => x.Customer)
            .ToArrayAsync();

        return customersWithoutOrders;
    }
}