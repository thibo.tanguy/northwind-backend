using Microsoft.EntityFrameworkCore;
using northwind_backend.App.Contexts;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Repositories.Implementations;

public class CategoriesRepository(ApplicationDbContext dbContext) : ICategoriesRepository
{
    public async Task<CategoryProductCountDto[]> GetCategoriesWithProductCountGreaterThanAsync(int productCount)
    {
        var categoriesWithCounts = await dbContext.Products
            .GroupBy(product => new { product.Category.Id, product.Category.CategoryName })
            .Select(categoryProductGroup => new CategoryProductCountDto
            {
                CategoryId = categoryProductGroup.Key.Id,
                CategoryName = categoryProductGroup.Key.CategoryName,
                ProductCount = categoryProductGroup.Count(),
            })
            .Where(c => c.ProductCount > productCount)
            .ToArrayAsync();

        return categoriesWithCounts;
    }
}