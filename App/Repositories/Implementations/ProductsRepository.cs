using Microsoft.EntityFrameworkCore;
using northwind_backend.App.Contexts;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Models.Entities;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Repositories.Implementations;

public class ProductsRepository(ApplicationDbContext dbContext) : IProductsRepository
{
    public async Task<Product[]> GetAllAsync()
    {
        return await dbContext.Products
            .Include(product => product.Category)
            .Include(product => product.Supplier)
            .ToArrayAsync();
    }

    public async Task<Product[]> GetProductsByCategoriesDtoAsync(CategoryDto[] categories)
    {
        if (categories.Length == 0) return [];
        var categoryIds = categories.Select(c => c.Id).ToArray();

        var products = await dbContext.Products
            .Where(product => categoryIds.Contains(product.CategoryId))
            .Include(product => product.Category)
            .Include(product => product.Supplier)
            .ToArrayAsync();

        return products;
    }

    public async Task<ProductWithAverageDto[]> GetProductsWithAveragePriceAsync()
    {
        var productWeightedAverages = await dbContext.OrderDetails
            .GroupBy(od => od.Product)
            .Select(group => new ProductWithAverageDto
            {
                ProductId = group.Key.Id,
                ProductName = group.Key.ProductName,
                AveragePrice = group.Sum(od => od.Quantity * od.UnitPrice) / group.Sum(od => od.Quantity)
            })
            .ToArrayAsync();

        return productWeightedAverages;
    }
}