using Microsoft.EntityFrameworkCore;
using northwind_backend.App.Contexts;
using northwind_backend.App.Models.Dtos;
using northwind_backend.App.Repositories.Interfaces;

namespace northwind_backend.App.Repositories.Implementations;

public class OrdersRepository(ApplicationDbContext dbContext) : IOrdersRepository
{
    public async Task<OrderTotalDto[]> GetOrdersWithTotalAmountAsync()
    {
        var orderTotals = await dbContext.Orders
            .Select(order => new OrderTotalDto
            {
                OrderId = order.Id,
                TotalAmount = order.OrderDetails.Quantity * order.OrderDetails.UnitPrice * (1 - (decimal)order.OrderDetails.Discount)
            })
            .ToArrayAsync();

        return orderTotals;
    }
}